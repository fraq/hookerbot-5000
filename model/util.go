package model

import (
	"crypto/sha1"
	"fmt"
	"math/rand"
	"time"
)

func GetMockPushHook(commitCount int) PushHook {
	return PushHook{
		ObjectKind:        "push",
		Before:            "95790bf891e76fee5e1747ab589903a6a1f80f22",
		After:             "da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
		Ref:               "refs/heads/master",
		CheckoutSha:       "da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
		UserID:            4,
		UserName:          "John Smith",
		UserUsername:      "jsmith",
		UserEmail:         "john@example.com",
		UserAvatar:        "https://s.gravatar.com/avatar/d4c74594d841139328695756648b6bd6?s=8://s.gravatar.com/avatar/d4c74594d841139328695756648b6bd6?s=80",
		ProjectID:         15,
		Project:           GetMockProject(),
		Commits:           GetMockCommits(commitCount),
		TotalCommitsCount: commitCount,
	}
}

func GetMockTagPushHook() TagPushHook {
	return TagPushHook{
		ObjectKind:  "tag_push",
		Ref:         "refs/tags/v1.0.0",
		CheckoutSha: "da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
		UserName:    "John Smith",
		Project:     GetMockProject(),
	}
}

func GetMockMergeRequestHook(action string) MergeRequestHook {
	project := GetMockProject()
	return MergeRequestHook{
		User: User{
			Username: "jsmith",
		},
		Project: project,
		ObjectAttributes: ObjectAttributes{
			Title:        "Test merge request",
			Action:       action,
			Iid:          23,
			Source:       project,
			SourceBranch: "some-feature",
			TargetBranch: "master",
			URL:          "http://example.com/diaspora/merge_requests/23",
		},
	}
}

func GetMockIssueHook(action string) IssueHook {
	return IssueHook{
		ObjectKind: "issue",
		User: User{
			Username: "jsmith",
		},
		ObjectAttributes: ObjectAttributes{
			Title:  "Test Issue",
			Action: action,
			Iid:    23,
			URL:    "http://example.com/diaspora/issues/23",
		},
	}
}

func GetMockWikiPageHook(action string) WikiPageHook {
	return WikiPageHook{
		ObjectKind: "wiki_page",
		Project:    GetMockProject(),
		User: User{
			Username: "jsmith",
		},
		ObjectAttributes: ObjectAttributes{
			Title:   "Test Wiki Page",
			Message: "Do something to a page",
			Action:  action,
			URL:     "http://example.com/root/awesome-project/wikis/awesome",
		},
	}
}

func GetMockPipelineHook(action string) PipelineHook {
	return PipelineHook{
		ObjectKind: "pipeline",
		Project:    GetMockProject(),
		User: User{
			Username: "jsmith",
		},
		ObjectAttributes: ObjectAttributes{
			ID:       10,
			Ref:      "master",
			Status:   action,
			Duration: 10,
		},
	}
}

func GetMockJobHook(action string) JobHook {
	return JobHook{
		ObjectKind:  "job",
		ProjectName: "Diaspora",
		User: User{
			Username: "jsmith",
		},
		JobID:     10,
		Ref:       "master",
		JobStatus: action,
		JobName:   "test",
	}
}

func GetMockProject() Project {
	return Project{
		Name:        "Diaspora",
		Description: "",
		WebURL:      "http://example.com/mike/diaspora",
		Homepage:    "http://example.com/mike/diaspora",
	}
}

func GetMockCommits(count int) []Commit {
	rand.Seed(time.Now().UnixNano())
	var commits []Commit
	for i := 0; i < count; i++ {
		hash := newSHA1Hash(32)
		commit := Commit{
			Sha:     hash,
			Message: "test-commit",
			URL:     "http://example.com/mike/diaspora/commit/" + hash,
		}
		commits = append(commits, commit)
	}

	return commits
}

func newSHA1Hash(n int) string {
	var randString string
	if n > 0 {
		randString = randomString(n)
	} else {
		randString = randomString(32)
	}

	hash := sha1.New()
	hash.Write([]byte(randString))
	bs := hash.Sum(nil)

	return fmt.Sprintf("%x", bs)
}

func randomString(n int) string {
	characterRunes := []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
	b := make([]rune, n)
	for i := range b {
		b[i] = characterRunes[rand.Intn(len(characterRunes))]
	}
	return string(b)
}
