package model

import (
	"time"
)

type Project struct {
	ID                int    `json:"id"`
	Name              string `json:"name"`
	Description       string `json:"description"`
	WebURL            string `json:"web_url"`
	AvatarURL         string `json:"avatar_url"`
	GitSSHURL         string `json:"git_ssh_url"`
	GitHTTPURL        string `json:"git_http_url"`
	Namespace         string `json:"namespace"`
	VisibilityLevel   int    `json:"visibility_level"`
	PathWithNamespace string `json:"path_with_namespace"`
	DefaultBranch     string `json:"default_branch"`
	Homepage          string `json:"homepage"`
	URL               string `json:"url"`
	SSHURL            string `json:"ssh_url"`
	HTTPURL           string `json:"http_url"`
}

type Repository struct {
	Name            string `json:"name"`
	URL             string `json:"url"`
	Description     string `json:"description"`
	Homepage        string `json:"homepage"`
	GitHTTPURL      string `json:"git_http_url"`
	GitSSHURL       string `json:"git_ssh_url"`
	VisibilityLevel int    `json:"visibility_level"`
}

type Commit struct {
	Added       []string  `json:"added"`
	Author      Author    `json:"author"`
	AuthorEmail string    `json:"author_email"`
	AuthorName  string    `json:"author_name"`
	Duration    int       `json:"duration"`
	FinishedAt  string    `json:"finished_at"`
	ID          int       `json:"id"`
	Message     string    `json:"message"`
	Modified    []string  `json:"modified"`
	Removed     []string  `json:"removed"`
	Sha         string    `json:"sha"`
	StartedAt   string    `json:"started_at"`
	Status      string    `json:"status"`
	Timestamp   time.Time `json:"timestamp"`
	URL         string    `json:"url"`
}

type Author struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}

type User struct {
	Name      string `json:"name"`
	Username  string `json:"username"`
	AvatarURL string `json:"avatar_url"`
}

type Issue struct {
	ID          int       `json:"id"`
	Title       string    `json:"title"`
	AssigneeIDs []int     `json:"assignee_ids"`
	AssigneeID  int       `json:"assignee_id"`
	AuthorID    int       `json:"author_id"`
	ProjectID   int       `json:"project_id"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
	Position    int       `json:"position"`
	BranchName  string    `json:"branch_name"`
	Description string    `json:"description"`
	MilestoneID int       `json:"milestone_id"`
	State       string    `json:state"`
	Iid         int       `json:iid"`
	Labels      []Label   `json:"labels"`
}

type ObjectAttributes struct {
	Action          string    `json:"action"`
	Assignee        User      `json:"assignee"`
	AssigneeID      int       `json:"assignee_id"`
	AssigneeIds     []int     `json:"assignee_ids"`
	Attachment      string    `json:"attachment"`
	AuthorID        int       `json:"author_id"`
	BeforeSha       string    `json:"before_sha"`
	BranchName      string    `json:"branch_name"`
	CommitID        string    `json:"commit_id"`
	Content         string    `json:"content"`
	CreatedAt       string    `json:"created_at"`
	Description     string    `json:"description"`
	Duration        int       `json:"duration"`
	FinishedAt      string    `json:"finished_at"`
	Format          string    `json:"format"`
	ID              int       `json:"id"`
	Iid             int       `json:"iid"`
	LastCommit      Commit    `json:"last_commit"`
	LineCode        string    `json:"line_code"`
	MergeStatus     string    `json:"merge_status"`
	Message         string    `json:"message"`
	MilestoneID     int       `json:"milestone_id"`
	Note            string    `json:"note"`
	NoteableID      int       `json:"noteable_id"`
	NoteableType    string    `json:"noteable_type"`
	Position        int       `json:"position"`
	ProjectID       int       `json:"project_id"`
	Ref             string    `json:"ref"`
	Sha             string    `json:"sha"`
	Slug            string    `json:"slug"`
	Source          Project   `json:"source"`
	SourceBranch    string    `json:"source_branch"`
	SourceProjectID int       `json:"source_project_id"`
	Stages          []string  `json:"stages"`
	State           string    `json:"state"`
	Status          string    `json:"status"`
	System          bool      `json:"system"`
	Tag             bool      `json:"tag"`
	Target          Project   `json:"target"`
	TargetBranch    string    `json:"target_branch"`
	TargetProjectID int       `json:"target_project_id"`
	Title           string    `json:"title"`
	URL             string    `json:"url"`
	UpdatedAt       time.Time `json:"updated_at"`
	WorkInProgress  bool      `json:"work_in_progress"`
	Variables       []struct {
		Key   string `json:"key"`
		Value string `json:"value"`
	} `json:"variables"`
	StDiff struct {
		Diff        string `json:"diff"`
		NewPath     string `json:"new_path"`
		OldPath     string `json:"old_path"`
		AMode       string `json:"a_mode"`
		BMode       string `json:"b_mode"`
		NewFile     bool   `json:"new_file"`
		RenamedFile bool   `json:"renamed_file"`
		DeletedFile bool   `json:"deleted_file"`
	} `json:"st_diff"`
}

type Label struct {
	ID          int       `json:"id"`
	Title       string    `json:"title"`
	Color       string    `json:"color"`
	ProjectID   int       `json:"project_id"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
	Template    bool      `json:"template"`
	Description string    `json:"description"`
	Type        string    `json:"type"`
	GroupID     int       `json:"group_id"`
}

type Snippet struct {
	ID              int       `json:"id"`
	Title           string    `json:"title"`
	Content         string    `json:"content"`
	AuthorID        int       `json:"author_id"`
	ProjectID       int       `json:"project_id"`
	CreatedAt       time.Time `json:"created_at"`
	UpdatedAt       time.Time `json:"updated_at"`
	FileName        string    `json:"file_name"`
	ExpiresAt       time.Time `json:"expires_at"`
	Type            string    `json:"type"`
	VisibilityLevel int       `json:"visibility_level"`
}

type MergeRequest struct {
	ID              int     `json:"id"`
	TargetBranch    string  `json:"target_branch"`
	SourceBranch    string  `json:"source_branch"`
	SourceProjectID int     `json:"source_project_id"`
	AuthorID        int     `json:"author_id"`
	AssigneeID      int     `json:"assignee_id"`
	Title           string  `json:"title"`
	CreatedAt       string  `json:"created_at"`
	UpdatedAt       string  `json:"updated_at"`
	MilestoneID     int     `json:"milestone_id"`
	State           string  `json:"state"`
	MergeStatus     string  `json:"merge_status"`
	TargetProjectID int     `json:"target_project_id"`
	Iid             int     `json:"iid"`
	Description     string  `json:"description"`
	Position        int     `json:"position"`
	Source          Project `json:"source"`
	Target          Project `json:"target"`
	LastCommit      Commit  `json:"last_commit"`
	WorkInProgress  bool    `json:"work_in_progress"`
	Assignee        User    `json:"assignee"`
}

type Build struct {
	ID            int    `json:"id"`
	Stage         string `json:"stage"`
	Name          string `json:"name"`
	Status        string `json:"status"`
	CreatedAt     string `json:"created_at"`
	StartedAt     string `json:"started_at"`
	FinishedAt    string `json:"finished_at"`
	When          string `json:"when"`
	Manual        bool   `json:"manual"`
	User          User   `json:"user"`
	Runner        string `json:"runner"`
	ArtifactsFile struct {
		Filename string `json:"filename"`
		Size     string `json:"size"`
	} `json:"artifacts_file"`
}

type Changes struct {
	UpdatedByID struct {
		Previous interface{} `json:"previous"`
		Current  int         `json:"current"`
	} `json:"updated_by_id"`
	UpdatedAt struct {
		Previous string `json:"previous"`
		Current  string `json:"current"`
	} `json:"updated_at"`
	Labels struct {
		Previous []Label `json:"previous"`
		Current  []Label `json:"current"`
	} `json:"labels"`
}

type Wiki struct {
	WebURL            string `json:"web_url"`
	GitSSHURL         string `json:"git_ssh_url"`
	GitHTTPURL        string `json:"git_http_url"`
	PathWithNamespace string `json:"path_with_namespace"`
	DefaultBranch     string `json:"default_branch"`
}

type PushHook struct {
	ObjectKind        string     `json:"object_kind"`
	Before            string     `json:"before"`
	After             string     `json:"after"`
	Ref               string     `json:"ref"`
	CheckoutSha       string     `json:"checkout_sha"`
	UserID            int        `json:"user_id"`
	UserName          string     `json:"user_name"`
	UserUsername      string     `json:"user_username"`
	UserEmail         string     `json:"user_email"`
	UserAvatar        string     `json:"user_avatar"`
	ProjectID         int        `json:"project_id"`
	Project           Project    `json:"project"`
	Repository        Repository `json:"repository"`
	Commits           []Commit   `json:"commits"`
	TotalCommitsCount int        `json:"total_commits_count"`
}

type TagPushHook struct {
	ObjectKind        string     `json:"object_kind"`
	Before            string     `json:"before"`
	After             string     `json:"after"`
	Ref               string     `json:"ref"`
	CheckoutSha       string     `json:"checkout_sha"`
	UserID            int        `json:"user_id"`
	UserName          string     `json:"user_name"`
	UserAvatar        string     `json:"user_avatar"`
	ProjectID         int        `json:"project_id"`
	Project           Project    `json:"project"`
	Repository        Repository `json:"repository"`
	Commits           []Commit   `json:"commits"`
	TotalCommitsCount int        `json:"total_commits_count"`
}

type IssueHook struct {
	ObjectKind       string           `json:"object_kind"`
	User             User             `json:"user"`
	Project          Project          `json:"project"`
	Repository       Repository       `json:"repository"`
	ObjectAttributes ObjectAttributes `json:"object_attributes"`
	Assignees        []User           `json:"assignees"`
	Assignee         User             `json:"assignee"`
	Labels           []Label          `json:"labels"`
	Changes          Changes          `json:"changes"`
}

type NoteHook struct {
	ObjectKind       string           `json:"object_kind"`
	User             User             `json:"user"`
	ProjectID        int              `json:"project_id"`
	Project          Project          `json:"project"`
	Repository       Repository       `json:"repository"`
	ObjectAttributes ObjectAttributes `json:"object_attributes"`
	Commit           Commit           `json:"commit"`
	MergeRequest     MergeRequest     `json:merge_request"`
	Issue            Issue            `json:issue"`
	Snippet          Snippet          `json:snippet"`
}

type MergeRequestHook struct {
	ObjectKind       string           `json:"object_kind"`
	User             User             `json:"user"`
	Project          Project          `json:"project"`
	Repository       Repository       `json:"repository"`
	ObjectAttributes ObjectAttributes `json:"object_attributes"`
	Labels           []Label          `json:"labels"`
	Changes          Changes          `json:"changes"`
}

type WikiPageHook struct {
	ObjectKind       string           `json:"object_kind"`
	User             User             `json:"user"`
	Project          Project          `json:"project"`
	Wiki             Wiki             `json:"wiki"`
	ObjectAttributes ObjectAttributes `json:"object_attributes"`
}

type PipelineHook struct {
	ObjectKind       string           `json:"object_kind"`
	ObjectAttributes ObjectAttributes `json:"object_attributes"`
	User             User             `json:"user"`
	Project          Project          `json:"project"`
	Commit           Commit           `json:"commit"`
	Builds           []Build          `json:"builds"`
}

type JobHook struct {
	ObjectKind       string     `json:"object_kind"`
	Ref              string     `json:"ref"`
	Tag              bool       `json:"tag"`
	BeforeSha        string     `json:"before_sha"`
	Sha              string     `json:"sha"`
	JobID            int        `json:"job_id"`
	JobName          string     `json:"job_name"`
	JobStage         string     `json:"job_stage"`
	JobStatus        string     `json:"job_status"`
	JobStartedAt     string     `json:"job_started_at"`
	JobFinishedAt    string     `json:"job_finished_at"`
	JobDuration      int        `json:"job_duration"`
	JobAllowFailure  bool       `json:"job_allow_failure"`
	JobFailureReason string     `json:"job_failure_reason"`
	ProjectID        int        `json:"project_id"`
	ProjectName      string     `json:"project_name"`
	User             User       `json:"user"`
	Commit           Commit     `json:"commit"`
	Repository       Repository `json:"repository"`
}
