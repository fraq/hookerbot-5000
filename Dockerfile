FROM golang:1.12-alpine as builder

RUN adduser -D -g 'hookerbot' hookerbot
WORKDIR /app
COPY . .
RUN apk add --no-cache git gcc
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -a -ldflags "-s -w -extldflags '-static'" -o ./opt/hookerbot

FROM scratch
EXPOSE 8080
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /app/opt/hookerbot /opt/hookerbot
USER hookerbot
ENTRYPOINT ["/opt/hookerbot"]
