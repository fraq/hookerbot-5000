module gitlab.com/fraq/hookerbot-5000

go 1.12

require (
	github.com/gin-gonic/gin v1.4.0
	github.com/inconshreveable/log15 v0.0.0-20180818164646-67afb5ed74ec // indirect
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.4.0
	github.com/stretchr/testify v1.3.0
	github.com/whyrusleeping/hellabot v0.0.0-20190117161550-dedc83c4926a
	gopkg.in/inconshreveable/log15.v2 v2.0.0-20180818164646-67afb5ed74ec
	gopkg.in/sorcix/irc.v1 v1.1.4 // indirect
)
