// Copyright © 2019 Bren Briggs <code@fraq.io>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package hookerbot

import (
	"gitlab.com/fraq/hookerbot-5000/model"
)

//  validate uses the interface type to decide what validation rules need to run
func validate(e interface{}) bool {
	switch e.(type) {
	case model.PushHook:
		return validatePushHook(e)
	default:
		return true
	}
}

// validatePushEvent is a set of rules to verify that the push events messages can be sent to clients
func validatePushHook(e interface{}) bool {
	event := e.(model.PushHook)
	isValid := true

	// verify that we have a count above 0
	if event.TotalCommitsCount < 1 {
		isValid = false
	}

	return isValid
}
