package hookerbot

import (
	"os"

	hbot "github.com/whyrusleeping/hellabot"
	log "gopkg.in/inconshreveable/log15.v2"
)

func (h *Hookerbot) NewBot(c Config) *hbot.Bot {
	chans := func(bot *hbot.Bot) {
		bot.Channels = c.Channels
	}
	sslOptions := func(bot *hbot.Bot) {
		bot.SSL = true
	}

	irc, err := hbot.NewBot(c.IRCServer, c.Nick, chans, sslOptions)

	if err != nil {
		log.Error(err.Error())
		os.Exit(1)
	}
	return irc
}
