// Copyright © 2019 Bren Briggs <code@fraq.io>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package hookerbot

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/fraq/hookerbot-5000/model"
)

func TestHandlePushHook(t *testing.T) {
	expected := "jsmith pushed 10 new commits to Diaspora (http://example.com/mike/diaspora) on master"
	e := model.GetMockPushHook(10)
	resp := handlePushHook(e)

	assert.Equal(t, expected, resp)
}

func TestHandleTagPushHook(t *testing.T) {
	expected := "John Smith pushed a new tag v1.0.0 to Diaspora (http://example.com/mike/diaspora) on sha da1560886d4f094c3e6c9ef40349f7d38b5d27d7"
	e := model.GetMockTagPushHook()
	resp := handleTagPushHook(e)

	assert.Equal(t, expected, resp)
}

func TestHandleIssueHook(t *testing.T) {
	actions := []string{"open", "update", "delete"}
	verbs := []string{"opened", "updated", "deleted"}
	expected := "Issue: Test Issue by jsmith (!23) %s. http://example.com/diaspora/issues/23"
	for i := range actions {
		e := model.GetMockIssueHook(actions[i])
		resp := handleIssueHook(e)
		assert.Equal(t, fmt.Sprintf(expected, verbs[i]), resp)
	}
}

func TestHandleMergeRequestHook(t *testing.T) {
	actions := []string{"open", "update", "delete"}
	verbs := []string{"opened", "updated", "deleted"}
	expected := "Merge Request: Test merge request by jsmith (!23) %s. http://example.com/diaspora/merge_requests/23"
	for i := range actions {
		e := model.GetMockMergeRequestHook(actions[i])
		resp := handleMergeRequestHook(e)
		assert.Equal(t, fmt.Sprintf(expected, verbs[i]), resp)
	}
}

func TestHandleWikiPageHook(t *testing.T) {
	actions := []string{"create", "update", "delete"}
	verbs := []string{"created", "updated", "deleted"}
	expected := "jsmith %s wiki page Test Wiki Page (http://example.com/root/awesome-project/wikis/awesome)"
	for i := range actions {
		e := model.GetMockWikiPageHook(actions[i])
		resp := handleWikiPageHook(e)
		assert.Equal(t, fmt.Sprintf(expected, verbs[i]), resp)
	}
}

func TestPipelineHook(t *testing.T) {
	actions := []string{"pending", "running", "success", "failure"}
	expected := "Pipeline 10 %s in project Diaspora (10 seconds)"
	for i := range actions {
		e := model.GetMockPipelineHook(actions[i])
		resp := handlePipelineHook(e)
		assert.Equal(t, fmt.Sprintf(expected, actions[i]), resp)
	}

}

func TestJobHook(t *testing.T) {
	actions := []string{"created", "pending", "running", "success", "failure"}
	expected := "Job 'test' (10) %s in project Diaspora"
	for i := range actions {
		e := model.GetMockJobHook(actions[i])
		resp := handleJobHook(e)
		assert.Equal(t, fmt.Sprintf(expected, actions[i]), resp)
	}

}
