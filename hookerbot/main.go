// Copyright © 2019 Bren Briggs <code@fraq.io>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package hookerbot

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	hbot "github.com/whyrusleeping/hellabot"
	"gitlab.com/fraq/hookerbot-5000/model"
	//log "gopkg.in/inconshreveable/log15.v2"
)

type Config struct {
	Channels  []string
	Nick      string
	IRCServer string
}

type Hookerbot struct {
	HTTP  *gin.Engine
	IRC   *hbot.Bot
	Token string
}

func (h *Hookerbot) Run() {
	go h.IRC.Run()
	h.HTTP.Run()
}

func (h *Hookerbot) eventHandler(ctx *gin.Context) {
	if ctx.Request.Header["X-Gitlab-Token"][0] != h.Token {
		ctx.JSON(403, model.Reply{
			Message: "Unauthorized: Invalid Token",
		})
		return
	}

	buf := make([]byte, 100000)
	num, _ := ctx.Request.Body.Read(buf)
	reqBody := buf[0:num]
	_, err := h.unmarshalWebhook(reqBody)

	if err != nil {
		ctx.JSON(http.StatusBadRequest, model.Reply{
			Message: err.Error(),
		})
		fmt.Println(err)
		return
	}
	ctx.JSON(http.StatusOK, reqBody)
}

func (h *Hookerbot) unmarshalWebhook(body []byte) (interface{}, error) {
	var unmarshaledBody map[string]interface{}
	if err := json.Unmarshal(body, &unmarshaledBody); err != nil {
		panic(err)
	}
	fmt.Println(unmarshaledBody["object_kind"])
	switch unmarshaledBody["object_kind"] {
	case "push":
		var e model.PushHook
		err := json.Unmarshal(body, &e)
		go h.reportEvent(e)
		return e, err
	case "issue":
		//fmt.Println((string(body)))
		var e model.IssueHook
		err := json.Unmarshal(body, &e)
		go h.reportEvent(e)
		return e, err
	case "merge_request":
		var e model.MergeRequestHook
		err := json.Unmarshal(body, &e)
		go h.reportEvent(e)
		return e, err
	case "pipeline":
		var e model.PipelineHook
		err := json.Unmarshal(body, &e)
		go h.reportEvent(e)
		return e, err

	default:
		fmt.Println("sflkjsfjwer")
	}
	return nil, nil
}

func (h *Hookerbot) reportEvent(e interface{}) {
	var resp string
	switch e.(type) {
	case model.PushHook:
		if validate(e) {
			resp = handlePushHook(e.(model.PushHook))
		}
	case model.TagPushHook:
		if validate(e) {
			resp = handleTagPushHook(e.(model.TagPushHook))
		}
	case model.IssueHook:
		if validate(e) {
			resp = handleIssueHook(e.(model.IssueHook))
		}
	case model.NoteHook:
		if validate(e) {
			resp = handleNoteHook(e.(model.NoteHook))
		}
	case model.MergeRequestHook:
		if validate(e) {
			resp = handleMergeRequestHook(e.(model.MergeRequestHook))
		}
	case model.WikiPageHook:
		if validate(e) {
			resp = handleWikiPageHook(e.(model.WikiPageHook))
		}
	case model.PipelineHook:
		if validate(e) {
			resp = handlePipelineHook(e.(model.PipelineHook))
		}
	case model.JobHook:
		if validate(e) {
			resp = handleJobHook(e.(model.JobHook))
		}
	default:
		resp = "Received event but could not parse it."
	}

	for _, channel := range h.IRC.Channels {
		h.IRC.Msg(channel, resp)
	}

}

// createMessage returns a formated string for a given string and arguments
func createMessage(msg string, args []interface{}) string {
	// may want to do other checks & stuff here
	return fmt.Sprintf(msg, args...)
}
