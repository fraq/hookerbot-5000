package hookerbot

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/fraq/hookerbot-5000/model"
)

func (h *Hookerbot) NewRouter(c Config) *gin.Engine {
	rtr := gin.Default()
	v1 := rtr.Group("/api/v1")
	{
		v1.GET("/ping", h.ping)
		v1.POST("/webhooks/hookerbot", h.eventHandler)
	}
	return rtr
}

func (h *Hookerbot) ping(ctx *gin.Context) {
	ctx.JSON(200, model.Reply{
		Message: "pong",
	})
}

func (h *Hookerbot) event(ctx *gin.Context) {
	var e model.PushHook
	if err := ctx.ShouldBindJSON(&e); err != nil {
		ctx.JSON(http.StatusBadRequest, model.Reply{
			Message: err.Error(),
		})
		return
	}
	ctx.JSON(http.StatusOK, model.Reply{
		Message: "Event received",
	})
	fmt.Printf("%+v\n", e)
}
