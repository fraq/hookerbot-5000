// Copyright © 2019 Bren Briggs <code@fraq.io>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package hookerbot

import (
	"strings"

	"gitlab.com/fraq/hookerbot-5000/model"
)

func handlePushHook(e model.PushHook) string {
	msgString := "%s pushed %d new commits to %s (%s) on %s"
	splitRef := strings.Split(e.Ref, "/")
	branch := splitRef[len(splitRef)-1]
	values := []interface{}{
		e.UserUsername,
		e.TotalCommitsCount,
		e.Project.Name,
		e.Project.WebURL,
		branch,
	}
	return createMessage(msgString, values)
}

func handleTagPushHook(e model.TagPushHook) string {
	msgString := "%s pushed a new tag %s to %s (%s) on sha %s"
	splitRef := strings.Split(e.Ref, "/")
	ref := splitRef[len(splitRef)-1]
	values := []interface{}{
		e.UserName,
		ref,
		e.Project.Name,
		e.Project.WebURL,
		e.CheckoutSha,
	}
	return createMessage(msgString, values)
}

func handleIssueHook(e model.IssueHook) string {
	action := strings.TrimSuffix(e.ObjectAttributes.Action, "e") // handles actions like "delete" or "change"
	msgString := "Issue: %s by %s (!%d) %sed. %s"
	values := []interface{}{
		e.ObjectAttributes.Title,
		e.User.Username,
		e.ObjectAttributes.Iid,
		action,
		e.ObjectAttributes.URL,
	}
	return createMessage(msgString, values)
}

func handleNoteHook(e model.NoteHook) string {
	return "not yet implemented"
}

func handleMergeRequestHook(e model.MergeRequestHook) string {
	action := strings.TrimSuffix(e.ObjectAttributes.Action, "e") // handles actions like "delete" or "change"
	msgString := "Merge Request: %s by %s (!%d) %sed. %s"
	values := []interface{}{
		e.ObjectAttributes.Title,
		e.User.Username,
		e.ObjectAttributes.Iid,
		action,
		e.ObjectAttributes.URL,
	}
	return createMessage(msgString, values)
}

func handleWikiPageHook(e model.WikiPageHook) string {
	action := strings.TrimSuffix(e.ObjectAttributes.Action, "e") // handles actions like "delete" or "change"
	msgString := "%s %sed wiki page %s (%s)"
	values := []interface{}{
		e.User.Username,
		action,
		e.ObjectAttributes.Title,
		e.ObjectAttributes.URL,
	}
	return createMessage(msgString, values)
}

func handlePipelineHook(e model.PipelineHook) string {
	msgString := "Pipeline %d %s in project %s (%d seconds)"
	values := []interface{}{
		e.ObjectAttributes.ID,
		e.ObjectAttributes.Status,
		e.Project.Name,
		e.ObjectAttributes.Duration,
	}
	return createMessage(msgString, values)
}

func handleJobHook(e model.JobHook) string {
	msgString := "Job '%s' (%d) %s in project %s"
	values := []interface{}{
		e.JobName,
		e.JobID,
		e.JobStatus,
		e.ProjectName,
	}
	return createMessage(msgString, values)
}
